Pod::Spec.new do |s|

s.platform = :ios
s.ios.deployment_target = '8.0'
s.name = "ACBSwiftUtilities"
s.summary = "ACBSwiftUtilities provides utilities for swift projects"
s.requires_arc = true

s.version = "0.1.1"

s.license = { :type => "MIT", :file => "LICENSE" }

s.author = { "Alejandro Cardenas Barragan" => "aleks.c.barragan.dev@gmail.com" }

s.homepage = "https://bitbucket.org/ishuster23/acbswiftutilities"

s.source = { :git => "https://ishuster23@bitbucket.org/ishuster23/acbswiftutilities.git", :tag => "#{s.version}"}

s.framework = "UIKit"

s.source_files = "ACBSwiftUtilities/**/*.{swift}"

#s.resources = "ACBSwiftUtilities/**/*.{png,jpeg,jpg,storyboard,xib}"

end
